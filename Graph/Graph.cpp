/*
 * Graph Implementation in C++
 * Assumes graph is undirected and unweighted
*/
#include <iostream>
#include <vector>
#include <queue>
#include <climits>

/* Values for state during traversal */
#define UNDISCOVERED 0
#define DISCOVERED 1
#define PROCESSED 2

using namespace std;

class Graph {
private:
	struct edgeNode {					// Struct to represent an edge
		// Constuctor, initializes an edge with destination and weight
		edgeNode(int dest, int weight) {
			this->dest = dest; 
			this->weight = weight;
		}
		// Constructor, initializes an edge with destination (for unweighted graphs)
		edgeNode(int dest) { 
			this->dest = dest;
		}
		int dest;
		int weight;
	};

	int maxV;
	int nvertices, nedges;				// Number of vertices and edges
	int inGraphVertices;				// Vertices currently in graph
	vector<int> degree;					// Vector stores degree of each node
	bool directed;						// Specifies if graph is directed
	vector<vector<edgeNode> > adj_list; // Adjacency List
	vector<int> state;					// Stores state during traversal
	vector<bool> inGraph;				// Specifies if vertex i is in the graph
	vector<bool> inTree;					// Store nodes in Tree for MST
	vector<int> entry_time;				// Store entry time for DFS
	vector<int> exit_time;				// Store exit time for DFS
	vector<int> parent;					// Store parent of vertices.
	vector<int> distance;				// Store distance for shortest paths & MST

public:
	/* Constructor */
	Graph(int maxV, bool directed = false) {
		this->maxV = maxV;					
		nertices = 0;
		nedges = 0;
		this->directed = directed;
		degree = vector<int>(maxV+1);					
		adj_list = vector<vector<edgeNode> >(maxV+1);	
		inGraph = vector<bool>(maxV+1, false);
	}

	// Graph operator=(Graph g) 
	// 	Graph G(g.vertices(), g.directed());

	// }

	/* Destructor */
	~Graph() {
	}
	
	// UTILITY FUNCTIONS
	int vertices() {
		return nvertices;
	}

	int edges() {
		return nedges;
	}

	bool isDirected() {
		return directed; 
	}
	void printInTree() {
		for(int i=1; i<inTree.size(); i++)
			if(inGraph[i])
				cout << inTree[i] << " ";
		cout << endl;
	}

	void printInGraph() {
		for(int i=1; i<inGraph.size(); i++) 
			cout << inGraph[i] << " ";	
		cout << endl;
	}
	void printDistance() {
		for(int i=1; i<distance.size(); i++)
			if(inGraph[i])
				cout << distance[i] << " ";
		cout << endl;
	}
	void printParent() {
		for(int i=1; i<parent.size(); i++)
			cout << parent[i] << endl;
	}

	// PRIMITIVE OPERATIONS
	void addEdge(int x, int y, int weight = -1) {
		if(x < 1 || x >= maxV) {
			cout << "Error: Exceeding Max Vertices" << endl;
			return;
		}
		if(!inGraph[x]) {
			inGraph[x] = true;
			nvertices++;
		}
		if(!inGraph[y]) {
			inGraph[y] = true;
			nvertices++;
		}
		edgeNode edge(y, weight);
		adj_list[x].push_back(edge);
		/* Adds edge (y,x) if graph is undirected */
		if(!isDirected()) {
			edgeNode edge(x, weight);
			adj_list[y].push_back(edge);
		}
		++nedges;
	}

	/* printGraph: Prints the adjacency list of the graph */
	void printGraph() {
		for(int i=1; i<=maxV; i++) {
			if(inGraph[i]) {
				cout << i << ": ";
				vector<edgeNode> vertex = adj_list[i];
				for(int j=0; j<vertex.size(); j++)
					cout << vertex[j].dest << " ";
				cout << endl;
			}
		}
	}

	/* BFS: Prints BFS Traversal from vertex start */
	void BFS(int start) {
		state = vector<int>(maxV+1, UNDISCOVERED);
		distance = vector<int>(maxV+1, 99999999);
		parent = vector<int>(maxV+1, -1);
		queue<int> q;
		q.push(start);
		state[start] = DISCOVERED;
		distance[start] = 0;
		parent[start] = -1;
		while(!q.empty()) {
			int x = q.front();
			q.pop();
			for(int i=0; i<adj_list[x].size(); i++) {
				int y = adj_list[x][i].dest;
				if(state[y] == UNDISCOVERED) {
					parent[y] = start;
					state[y] = DISCOVERED;
					distance[y] = distance[start] + 1;
					q.push(y);
				}
				if(state[y] != PROCESSED)
					BFSProcessEgde(start, y);
			}
			state[x] =  PROCESSED;
			
		}
		state.clear();
		distance.clear();
		parent.clear();
	}
	// Process edge during BFS
	void BFSProcessEgde(int x, int y) {
		cout << "Edge: (" << x << "," << y << ")" << endl;
	}
	// Process edge during DFS
	void DFSProcessEdge() {
	}

	/* DFS: Prints DFS Traversal */
	void DFS() {
		/*if(!inGraph[start]) {
			cout << "DFS Error: Invalid Vertex" << endl;
			return; 
		}*/
		state = vector<int>(maxV, UNDISCOVERED);
		entry_time = vector<int>(maxV, 0); 
		exit_time = vector<int>(maxV, 0);
		parent = vector<int>(maxV, -1);
		int time = -1;
		// For each separated component
		for(int i=1; i<=maxV;  i++) 
			if(inGraph[i] && state[i] == UNDISCOVERED)
				DFS(i, time);

		// Clear initialized vectors
		clearUtil();
	}

	void DFS(int x, int& time) {
		state[x] = DISCOVERED;	// Mark current vertex as DISCOVERED
		entry_time[x] = ++time;	// Update entry time for vertex
		vector<edgeNode> adj_vertices = adj_list[x];
		for(int i=0; i<adj_vertices.size(); i++) {
			int y = adj_vertices[i].dest;
			// Recursively call DFS for all adjacent vertices
			if(state[y] == UNDISCOVERED) {
				cout << "Discovered Edge: (" << x << ", " << y << ")" << endl;
				parent[y] = x;
				DFS(y, time);
			}
			/*else if(state[y] != UNDISCOVERED) {
				if(parent[x] != y) {
					cout << "Cycle Exists from " << x << "to " << y << endl;
					return;
				}
			}*/
		}
		state[x] = PROCESSED;	// Mark state as PROCESSED
		exit_time[x] = ++time;	// Update exit time for vertex
	}

	/* If already seen node encountered node during DFS, thenn cycle exist 
	 * If node is not parent, then its back edge. 
	 * Check if the node is not the parent, if yes then cycle
	*/
	bool detectCycle() {
	}

	void primMST(int start) {
		parent = vector<int>(maxV, -1);
		inTree = vector<bool>(maxV, 0);
		state = vector<int>(maxV, UNDISCOVERED);
		distance = vector<int>(maxV, INT_MAX);

		state[start] = DISCOVERED;
		distance[start] = 0;
		parent[start] = -1;

		int x = start;
		while(!inTree[x]) {
			cout << "Node: " << x << endl;
			inTree[x] = true; 
			vector<edgeNode> adj_vertices = adj_list[x];
			// Update distance for adjacent nodes
			for(int i=0; i<adj_vertices.size(); i++) {
				int y = adj_vertices[i].dest;
				int weight = adj_vertices[i].weight;
				// cout << y << " " << weight << " " << distance[y] << endl;
				if(!inTree[y] && distance[y] > weight) {
					distance[y] = weight;
					parent[y] = x;
				}
			}
			// Select the next vertex to add
			int dist = INT_MAX;
			for(int i=1; i<=maxV; i++) {
				if(inGraph[i]) {
					if(!inTree[i] && distance[i] < dist) {
						dist = distance[i];
						x = i;
					}
				}
			}
		}
		// Print the Tree
		cout << "MST: " << endl;
		Graph mst(maxV);
		for(int i=1; i<=maxV; i++) {
			if(inGraph[i] && parent[i] != -1) {
				mst.addEdge(parent[i], i);
				cout << "Edge: (" << parent[i] << ", " << i << ") " << distance[i] << endl;
			}
		}
		mst.printGraph();
		clearUtil();
	}

	void kruskalMST(int start) {
	}

	void dijkstra(int s) {
		// Initialization
		cout << "Init" << endl;
		parent = vector<int>(maxV, -1);
		inTree = vector<bool>(maxV, false);
		distance = vector<int>(maxV, INT_MAX);

		distance[s] = 0;
		parent[s] = -1;
		cout << "Start Update" << endl;
		// Strating updating shortest distances
		int x = s;
		while(!inTree[x]) {
			cout << "Node: " << x << endl;
			inTree[x] = true;
			vector<edgeNode> adj_vertices = adj_list[x];
			for(int i=0; i<adj_vertices.size(); i++) {
				// Relax each adjacent node
				int y = adj_vertices[i].dest;
				int weight = adj_vertices[i].weight;
				if(distance[y] > distance[x] + weight) {
					distance[y] = distance[x] + weight;
					parent[y] = x;
				}
			}
			printDistance();

			// Select the next vertex (ie vertex with least distance)
			int dist = INT_MAX;
			int x = 1;
			for(int i=1; i<=maxV; i++) {
				if(inGraph[i]) {
					if(!inTree[i] && (distance[i] < dist)) {
						x = i;
						dist = distance[i];
					}
				}
			}
			cout << "x: " << x << endl;
			cout << "inTree(x): " << inTree[x] << endl; 
			printInTree();
		}
		cout << "End Update" << endl;

		// Print paths from s to all other vertices
		cout << "Paths: " << endl;
		cout << "Vertex Distance RevPath" << endl;
		for(int i=1; i<=maxV; i++) {
			if(inGraph[i]) {
				if(i == s) 
					continue;
				int y = i;
				cout << i << "\t\t";
				cout << distance[i] << "\t\t";
				if(parent[y] == -1) {
					cout << "---" << endl;
					continue;
				}
				while(parent[y] != -1 && parent[y] != s) {
					cout << parent[y]  << "->";
					y = parent[y];
				}
				cout << endl;
			}
		}
		clearUtil();
	}

	bool BellmanFord(int s) {
		cout << "Init" << endl;
		parent = vector<int>(maxV, -1);
		distance = vector<int>(maxV, INT_MAX);

		distance[s] = 0;
		parent[s] = -1;
		// Relax all edges 
		for(int n=1; n<=nvertices; n++) {
			// Relax All Edges
			for(int i=1; i<=maxV; i++) {
				if(inGraph[i]) {
					vector<int> adj_vertices = adj_list[i];
					for(int j=0; j<adj_vertices.size(); j++) {
						// RELAX(U, V, WEIGHT)
						int u = i;
						int v = adj_vertices[i].dest;
						int w = adj_vertices[i].weight;
						if(distance[v] > distance[u] + weight)
							distance[v] = distance[u] + weight;
					}
				}
			}
		}

		// Check for negative weight cycles
		

	}

	void floyd() {
	}

	// void util() {
	// 	for(int i=0; i<inGraph.size(); i++) 
	// 		cout << inGraph[i] << " ";
	// 	cout << endl;
	// }

	void clearUtil() {
		parent.clear();
		inTree.clear();
		distance.clear();
		state.clear();
		entry_time.clear();
		exit_time.clear();
	}
};

int main() {
	int nvertices = 100;
	// Undirected Graph
	// Graph ug(maxV);
	// ug.addEdge(1,2);
	// ug.addEdge(1,3);
	// ug.addEdge(1,4);
	// ug.addEdge(2,5);
	// ug.addEdge(3,5);
	// ug.addEdge(3,8);
	// ug.addEdge(3,6);
	// ug.addEdge(3,7);
	// ug.addEdge(4,8);
	// ug.addEdge(5,6);
	// ug.addEdge(6,9);
	// ug.addEdge(7,8);
	// ug.addEdge(7,9);
	// ug.util();
	// ug.printGraph();
	// cout << "BFS: ----------------------------------" << endl;
	// ug.BFS(1);
	// cout << "DFS: ----------------------------------" << endl;
	// ug.DFS();

	Graph prim(maxV);
	prim.addEdge(1,2,4);
	prim.addEdge(1,8,8);
	prim.addEdge(2,3,8);
	prim.addEdge(2,8,11);
	prim.addEdge(3,9,2);
	prim.addEdge(3,4,7);
	prim.addEdge(3,6,4);
	prim.addEdge(4,5,9);
	prim.addEdge(4,6,14);
	prim.addEdge(5,6,10);
	prim.addEdge(6,7,2);
	prim.addEdge(7,8,1);
	prim.addEdge(7,9,6);
	prim.addEdge(8,9,7);
	prim.printGraph(); 
	prim.printInGraph();
	prim.primMST(1);
	/*Graph prim(maxV);
	prim.addEdge(1,2,5);
	prim.addEdge(1,6,3);
	prim.addEdge(2,3,10);
	prim.addEdge(2,5,1);
	prim.addEdge(2,7,4);
	prim.addEdge(3,4,5);
	prim.addEdge(3,7,8);
	prim.addEdge(4,5,7);
	prim.addEdge(4,7,9);
	prim.addEdge(5,6,6);
	prim.addEdge(5,7,2);
	// prim.printGraph();
	prim.primMST(3);
*/	// Directed Graph
	// Dijkstra Shortest Path
	// Graph dij(maxV, true);
	// dij.addEdge(1,2,10);
	// dij.addEdge(1,4,5);
	// dij.addEdge(2,3,1);
	// dij.addEdge(2,4,2);
	// dij.addEdge(3,5,4);
	// dij.addEdge(4,2,3);
	// dij.addEdge(4,5,2);
	// dij.addEdge(4,3,9);
	// dij.addEdge(5,3,6);
	// dij.addEdge(5,1,7);
	// dij.printGraph();
	// dij.printInGraph();
	// dij.dijkstra(1);
	// Graph dg(maxV, true);
	// Graph Kruskal(maxV);
	// kruskal.addEdge();
	// kruskal.addEdge();
	// kruskal.addEdge();
	// kruskal.addEdge();
	// kruskal.addEdge();
	// kruskal.addEdge();
	// kruskal.addEdge();
	// kruskal.addEdge();
	// kruskal.addEdge();
	// kruskal.addEdge();
	// kruskal.addEdge();
	
	return 0;
}