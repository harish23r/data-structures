#include <iostream>
#include <vector>
using namespace std;

struct list {
	int data;
	list* next;
};

list* new_node(int val, list* next) {
	list* new_ = new list;
	new_->data = val;
	new_->next = next;
	return new_;
}

list* add_to_last(list* head, int val) {
	if(head == NULL) {
		head = new_node(val, NULL);
		return head;
	}
	else {
		list* temp = head;
		while(temp->next != NULL) 
			temp = temp->next;
		temp->next = new_node(val, NULL);
		return head;
	}
}

// list** splitList(list* l) 
vector<list*> splitList(list* l) {
	vector<list*> lists;
	lists.push_back(NULL);
	lists.push_back(NULL);
	int control = 1;
	list* temp = l;
	list* temp1 = lists[0];
	list* temp2 = lists[0];
	while(temp) {
		if(control) {
			if(!temp1) {
				lists[0] = new_node(temp->data, NULL);
				temp1 = lists[0];
			}
			else {
				temp1->next = new_node(temp->data, NULL);
				temp1 = temp1->next;
			}
			control = 0;
		}
		else {
			if(!temp2) {
				lists[0] = new_node(temp->data, NULL);
				temp1 = lists[0];
			}
			else {
				temp2->next = new_node(temp->data, NULL);
				temp2 = temp2->next;
			}
			control = 1;
		}
		temp = temp->next;
	}
	return lists;
}

void print(list* head) {
	while(head) {
		cout << head->data << " "; 
		head = head->next;
	}
	cout << endl;
}

int main() {
	list* head = NULL;
	head = add_to_last(head, 1);
	head = add_to_last(head, 2);
	head = add_to_last(head, 3);
	head = add_to_last(head, 4);
	head = add_to_last(head, 5);
	head = add_to_last(head, 6);
	head = add_to_last(head, 7);
	head = add_to_last(head, 8);
	head = add_to_last(head, 9);
	head = add_to_last(head, 10);
	print(head);
	vector<list*> lists = splitList(head);
	print(lists[0]);
	print(lists[1]);
	return 0;
}