#include <iostream>
#include <vector>
#include "LinkedList.h"
using namespace std;

int main() {
	List l;
	l.append(1);
	l.append(2);
	l.append(3);
	l.append(4);
	l.append(4);
	l.append(3);
	l.append(2);
	l.append(1);
	// l.append(5);
	// l.append(6);
	// l.append(7);
	// l.append(8);
	// l.append(9);
	// l.append(10);
	l.print();
	// l.delete_nth_last(4);
	// l.print();
	// l.delete_nth_last(3);
	// l.print();
	cout << l.isPalindrome() << endl;
	l.print();
	return 0;
}