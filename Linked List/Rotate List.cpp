#include <iostream>
#include "LinkedList.h"
using namespace std;

int main() {
    list* head = NULL;
    head = new_node(0, NULL);
    head->next = new_node(0, NULL);
    head->next->next = new_node(0, NULL);
    print(head);
    head = addOne(head);
    print(head);
    return 0;
}

ListNode* rotateLeft(ListNode* head, int k) {
    ListNode* slow = head;
    ListNode* fast = NULL;
    int len = length(head);
    if(k%len == 0 || head == NULL || head->next ==  NULL)
        return head;
    for(int i=0; i<(k%len)-1 && slow->next; i++)
        slow = slow->next;
    fast = slow;
    while(fast->next)
        fast = fast->next;
    ListNode* temp = slow->next;
    slow->next = NULL;
    fast->next = head;
    head = temp;
    return head;
}

ListNode* rotateRight(ListNode* head, int k) {
    ListNode *slow = head;
    ListNode *fast = head;
    int len = length(head);
    if(head == NULL || head->next == NULL || k%len == 0)
        return head;
    for(int i=0; i<k%len && fast->next; i++)
        fast = fast->next;
    while(fast->next) {
        slow = slow->next;
        fast = fast->next;
    }
    ListNode* temp = slow->next;
    slow->next = NULL;
    fast->next = head;
    head = temp;
    return head;
}