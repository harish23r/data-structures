#include <iostream>
#include <vector>
#include <stack>
using namespace std;

struct list {
	int data;
	list* next;
};

list* new_node(int val, list* next) {
	list* new_ = new list;
	new_->data = val;
	new_->next = next;
	return new_;
}

list* add_to_last(list* head, int val) {
	if(head == NULL) {
		head = new_node(val, NULL);
	}
	else {
		list* temp = head;
		while(temp->next != NULL) 
			temp = temp->next;
		temp->next = new_node(val, NULL);
	}
	return head;
}

void print(list* head) {
	while(head) {
		cout << head->data << " ";
		head = head->next;
	}
	cout << endl;
}

list* addTwoNumbers(list* l1, list* l2) {
	return NULL;
}

int plusOneCarry(list* head) {
	if(head == NULL)
		return 1;
	// int carry = 0;
	int sum = head->data + plusOneCarry(head->next);
	head->data = sum % 10;
	// carry = sum / 10; // return carry
	return sum/10;
}

list* addOne(list* head) {
	if(head == NULL) {
		head = new_node(1, NULL);
		return head;
	}
	int carry = plusOneCarry(head);
	if(carry) {
		head = new_node(carry, head);
	}
	return head;
}

// list** splitList(list* l) 
vector<list*> splitList(list* l) {
	vector<list*> lists;
	lists.push_back(NULL);
	lists.push_back(NULL);
	int control = 1;
	list* temp = l, *temp1 = NULL, *temp2 = NULL;
	while(temp) {
		if(control) {
			if(!temp1) {
				lists[0] = new_node(temp->data, NULL);
				temp1 = lists[0];
			}
			else {
				temp1->next = new_node(temp->data, NULL);
				temp1 = temp1->next;
			}
			control = 0;
		}
		else {
			if(!temp2) {
				lists[0] = new_node(temp->data, NULL);
				temp1 = lists[0];
			}
			else {
				temp2->next = new_node(temp->data, NULL);
				temp2 = temp2->next;
			}
			control = 1;
		}
		temp = temp->next;
		return lists;
	}
	return lists;
}

// Merge two sorted linked list to the first list
list* mergeSorted(list* a, list* b) {
	list *node1 = a, *node2 = b, *tail = a;
	if(node2 == NULL || node1 == NULL)
		return NULL;
	while(node2 != NULL) {
		while(node2->data < node1->data && node2 != NULL) {
			a = new_node(node2->data, a);
		}
	}
}

class List {
	struct list {
		int data;
		list* next;
	};
	list* head;
	list* tail;
	int size;

    list* new_node(int val) {
		list* node = new list;
		node->data = val;
		node->next = NULL;
		return node;
	}

	list* new_node(int val, list* next) {
		list* node = new list;
		node->data = val;
		node->next = next;
		return node;
	}

    void insert_at_begin(int val) {
		list* head_node = head;
		list* new_ = new_node(val, head);
		head = new_;
		if(tail == NULL)
			tail = head;
        size++;
	}

	void insert_at_end(int val) {
		list* new_ = new_node(val, NULL);
		if(tail != NULL)
            tail->next = new_;
		tail = new_;
		if(head == NULL)
			head = tail;
        size++;
	}

	void insert_at_n(int val, int pos) {
		list* node = head;
		// Move one before position required position
		int i;
		for(i=1; i<pos-1 && node!= NULL; i++) {
			node = node->next;
			cout << "Pos: " << i << " : " << node->data << endl;
		}
		if(node == NULL && i == pos-1) {
            list* new_ = new_node(val, node->next);
            node->next = new_;
            tail = new_;
            size++;
            return;
		}
		// If loop terminated because of end of list, error
		if(node == NULL) {
			std::cout << "Error: Invalid position" << std::endl;
			return;
		}
		// Insert a new node
		list* new_ = new_node(val, node->next);
		node->next = new_;
		// If node is at the end, update tail
		if(new_->next == NULL)
			tail = new_;
        size++;
	}

public:
	/* Constructor: Initialize head and tail to NULL */
	List() {
		std::cout << "Constructor" << std::endl;
		head = NULL;
		tail = NULL;
		size = 0;
	}

	/* Destructor: Deallocates all nodes */
	~List() {
		std::cout << "Destructor" << std::endl;
		list* node = head;
		while(node) {
            list* temp = node;
            node = node->next;
            delete temp;
		}
		head = NULL;
	}

    int size_() {
		return size;
	}

	/* Finds number of elements in the list */
	int length() {
		list* node = head;
		int count = 0;
		while(node) {
			++count;
			node = node->next;
		}
		return count;
	}

	/* Append element to end of the list */
	void append(int val) {
		insert_at_end(val);
	}

	/* Inserts value at the give position
	Returns pointer to that node */
	void insert(int val, int pos) {
		if(pos == 1 || head == NULL)
			insert_at_begin(val);
		else if(pos <= size)
			insert_at_n(val, pos);
        else
            cout << "Error: Position does not exist" << endl;
	}

	void delete_nth(int n) {
		int i = 1;
		list* node = head;
		if(head == NULL)
			return;
		if(n == 1) {
			list* temp = head;
			head = head->next;
			delete temp;
			size--;
			return;
		}
		while(node && i < n-1) {
			node = node->next;
			i++;
		}
		if(node == NULL)
			return; 
		else {
			list* temp = node->next;
			node->next = temp->next;
			delete temp;
			size--;
		}
	}

	void delete_nth_last(int n) {
		list* node = head;
		int len = 0;
		// Find length
		while(node) {
			len++;
			node = node->next;
		}
		node = head;
		list* fast_node = head;
		for(int i=0; i<n && fast_node; i++) {
			fast_node = fast_node->next;
		}
		if(fast_node == NULL)
			return;
		while(fast_node->next) {
			fast_node = fast_node->next;
			node = node->next;
		}
		if(node == NULL)
			return;
		list* temp = node->next;
		node->next = temp->next;
		delete temp;
		size--;
	}

    /* Deletes the first occurance of the given key */
	void delete_key(int val, bool all=false) {
		list* node = head;
		if(head == NULL)
			return;
		// Handle if node at head
		if(node->data == val) {
			head = head->next;
			delete node;
			size--;
			return;
		}
		// Move upto node before required node
		while(node->next && node->next->data != val)
			node = node->next;
		if(node->next == NULL) {
			std::cout << "Error: Element not found" << std::endl;
			return;
		}
		list* temp = node->next;
		node->next = node->next->next;
		delete temp;
		size--;
		// Update tail
		if(node->next == NULL) {
			tail = node;
		}
	}

	/* REverses the list in an iterative manner */
	void reverse() {

		list *curr = head, *prev = NULL, *next;
		while(curr) {
			next = curr->next;
			curr->next = prev;
			prev = curr;
			curr = next;
		}
		head = prev;
		// Update tail pointer
		curr = head;
		while(curr->next != NULL)
			curr = curr->next;
		curr = tail;
	}

	void insert_into_sorted(int val) {
		list* node = head;
		if(val <= node->data || node == NULL) {
			insert(val, 1);
			size++;
			return;
		}
		while(node->next && node->next->data < val) {
			node = node->next;
		}
		if(node->next == NULL) {
			node->next = new_node(val, NULL);
			size++;
			return; 
		}
		if(node->next->data >= val) {
			list* new_ = new_node(val, node->next);
			node->next = new_;
			size++;
			return;
		} 
	}

	// Swap nodes in list
	/*void swap_pair_nodes() {
		list *temp1, *temp2;
		list* node;
		temp1 = node;
		if(temp1)
		temp2 = node->next;
		// Swap the nodes
		next1 = temp2->next;
		temp2->next = temp1;
		temp1->next = next1;
		if(temp1 == head)
			head = temp2;

	}*/

	void remove_sorted_duplicates() {
		list* node = head;
		while(node) {
			int x = node->data;
			while(node->next && node->next->data == x) {
				list* temp = node->next;
				node->next = temp->next;
				delete temp;
				size--;
			}
			node = node->next;
		}
	}

	// Delete alternate nodes in list
	void delete_alternate() {
		list* node = head;
		while(node && node->next) {
			list* temp = node->next;
			node->next = temp->next;
			delete temp;
			node = node->next;
			size--;
		}
	}

	void last_to_front() { 
		list* node = head;
		if(node == NULL) 
			return;
		// Move upto one but last
		while(node->next && node->next->next) {
			node = node->next;
		}
		list* temp = node->next; 
		node->next = NULL;
		temp->next = head;
		head = temp;
	}

	list* findMiddle() {
		list *slow, *fast;
		slow = fast = head;
		while(fast && fast->next) {
			slow = slow->next;
			fast = fast->next->next;
		}
		return slow;
	}

	/*bool isPalindrome() {
		list* mid = findMiddle();
		stack<int> s;
		list* node = head;
		while(node && node != mid) {
			s.push(node->data);
			node = node->next; 
		}
		node = node->next;
		while(node != NULL) {
			if(s.top() == node->data)
				s.pop();
			node = node->next;
		}
		if(!s.empty())
			return false;
		else
			return true;
	}*/

	// bool isPalindrome() {
	// 	int len = length();
	//     list* mid = findMiddle();
	//     cout << mid->data << endl;
	//     cout << mid->next->data << endl;
	//     list* node = head;
	//     // ListNode* prev = NULL;
	//     while(node->next != mid) {
	//         node = node->next;
	//     }
	//     list* prev = node;
	//     list* curr = node->next;
	//     while(curr) {
	//         list* next = curr->next;
	//         curr->next = prev;
	//         prev = curr;
	//         curr = next;
	//     }
	//     print();
	//     mid->next = prev;
	//     list* node1 = mid->next;
	//     list* node2 = head;
	//     while(node1 && node2 && node1->data == node2->data) {
	//         node1 = node1->next;
	//         node2 = node2->next;
	//     }
	//     if(node1 == NULL && node2 == NULL)
	//         return 1;
	//     else
	//         return 0;
	// }

	void rotateLeft(int k) {
	    list* slow = head;
	    list* fast = NULL;
	    int len = length();
	    if(k%len == 0 || head == NULL || head->next ==  NULL)
	        return;
	    for(int i=0; i<(k%len)-1 && slow->next; i++)
	        slow = slow->next;
	    fast = slow;
	    while(fast->next)
	        fast = fast->next;
	    list* temp = slow->next;
	    slow->next = NULL;
	    fast->next = head;
	    head = temp;
    }

	void rotateRight(int k) {
	    list *slow = head;
	    list *fast = head;
	    int len = length();
	    if(head == NULL || head->next == NULL || k%len == 0)
	        return;
	    for(int i=0; i<k%len && fast->next; i++)
	        fast = fast->next;
	    while(fast->next) {
	        slow = slow->next;
	        fast = fast->next;
	    }
	    list* temp = slow->next;
	    slow->next = NULL;
	    fast->next = head;
	    head = temp;
	}

	void print() {
		list* node = head;
		while(node) {
			std::cout << node->data << " ";
			node = node->next;
		}
		std::cout << std::endl;
	}
};