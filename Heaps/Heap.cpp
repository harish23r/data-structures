#include <iostream>
#include <vector>
using namespace std;

class PriorityQueue {
	vector<int> heap;
	int size;

	int parent(int i) {
		return i <= 1 ? -1 : i/2;
		/*
		if(i <= 1)	return -1;
			return i/2;
		*/
	}

	int left_child(int i) {
		return 2*i;
	}

	int right_child(int i) {
		return 2*i + 1;
	}

	/*void percolate_down(int i) {
		if(left_child(i) <= size) {
			if(heap[left_child(i)] < heap[i] || ( right_child(i) <= size && heap[right_child(i)] < heap[i])) {
				if(right_child(i) <= size) {
					if(heap[left_child(i)] < heap[right_child(i)]) {
						swap(heap[i], heap[left_child(i)]);
						percolate_down(left_child(i));
					}
					else{
						swap(heap[i], heap[right_child(i)]);
						percolate_down(right_child(i));
					}
				}
				else if(heap[left_child(i)] < heap[i]) {
					swap(heap[i], heap[left_child(i)]);
					percolate_down(left_child(i));
				}
			}
			return;
		}
	}*/

	// MIN-HEAPIFY(NODE)
	void percolate_down(int n) {
		int x = heap[n], i, child;
		for(i=n; left_child(i) <= size; i = child) {
			child = left_child(i);
			if(child+1 <= size && heap[right_child(i)] < heap[left_child(i)])
				// Make child as right child
				child = child + 1;
			if(heap[child] < x)
				heap[i] = heap[child];
			else
				break;
		}
		heap[i] = x;
	}

public: 
	/* Constructor: Initializes Heap */
	PriorityQueue() {
		heap.push_back(-1);	// Sentinel to fill up unused index 0
		size = 0;
	}

	/* Constructor: Builds heap from a vector of ints */
	PriorityQueue(vector<int> nums) {
		heap.push_back(-1);	// Sentinel to fill up unused index 0
		int len = nums.size();
		for(int i=0; i<len; i++) {
			heap.push_back(nums[i]);
		}
		size = len;
		print();
		for(int i=size/2; i>0; i--)
			percolate_down(i);
		cout << "Created Heap: ";
		print();
		cout << "Size: " << size << endl;
	}

	~PriorityQueue() {
	}

	/* insert(int): Inserts element into heap */
	void insert(int key) {
		heap.push_back(key);
		size++;
		cout << size << endl;
		int i;
		// Bubble up
		for(i=size; heap[parent(i)] > key; i = parent(i))
			heap[i] = heap[parent(i)];
		heap[i] = key;
	}

	/* minElement(): Returns the minimum element */
	int minElement() {
		if(size > 0)
			return heap[1];
		else
			cout << "Error: Empty Heap!" << endl; 
	}

	/* extractMin() : Returns and removes the minimum element */
	int extractMin() {
		int min = heap[1];
		heap[1] = heap[size];
		size = size-1;
		percolate_down(1);
		return min;
	}

	/* decreaseKey(key, newkey): Decreases the value of key with new key */
	void decreaseKey(int key, int newkey) {
	}

	/* print(): Prints the heap */
	void print() {
		if(size > 0) {
			for(int i=1; i<=size; i++)
				cout << heap[i] << " ";
		}
		cout << endl;
	}
};

heapsort(vector<int> &nums) {
	// Build Heap
	cout << "Inside heap sort:" << endl;
	PriorityQueue heap(nums);
	nums.clear();
	while(!heap.empty())
		nums.push_back(heap.extractMin());
	cout << endl;
}


int main() {
	PriorityQueue heap;
	heap.insert(32);
	heap.insert(68);
	heap.insert(12);
	heap.insert(21);
	heap.insert(56);
	heap.insert(18);
	heap.print();
	cout << "-----------------------" << endl;
	vector<int> nums;
	nums.push_back(12);
	nums.push_back(15);
	nums.push_back(20);
	nums.push_back(22);
	nums.push_back(2);
	nums.push_back(4);
	nums.push_back(10);
	nums.push_back(7);
	nums.push_back(8);
	PriorityQueue heap2(nums);
	heap2.print();
	cout << "-----------------------" << endl;
	vector<int> snums;
	snums.push_back(6);
	snums.push_back(20);
	snums.push_back(45);
	snums.push_back(98);
	snums.push_back(14);
	snums.push_back(23);
	snums.push_back(56);
	snums.push_back(19);
	snums.push_back(48);
	heapsort(snums);
	for(int i=0; i<snums.size(); i++)
		cout << snums[i] << " ";
	return 0;
}