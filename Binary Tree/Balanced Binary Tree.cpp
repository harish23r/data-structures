/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    int findHeight(TreeNode* root, int height) {
        if(root == NULL)
            return 0;
        return max(findHeight(root->left, height), findHeight(root->right, height)) + 1;
    }
    
    bool isBalanced(TreeNode* root) {
        if(root == NULL)
            return true;
        if(abs(findHeight(root->left, 0) - findHeight(root->right, 0)) > 1)
            return false;
        return isBalanced(root->left) && isBalanced(root->right);
    }
};