#include <iostream>
#include <vector>
using namespace std;

struct TreeNode {
        int val;
        TreeNode* left;
        TreeNode* right;
    };

TreeNode* new_node(int k) {
        TreeNode* new_ = new TreeNode;
        new_->val = k;
        new_->left = new_->right = NULL;
        return new_;
    }

class Solution {
public:
    TreeNode* root;
    int num_nodes, max_count;
    vector<int> count; 

    

    Solution() {
    }

    
    
    
    void count_freq(TreeNode* root) {
        if(root == NULL)
            return;
        count[root->val]++;
        count_freq(root->left);
        count_freq(root->right);
    }

    TreeNode* findMin(TreeNode* root) {
        if(root == NULL)
            return NULL;
        if(root->left)
            return findMin(root->left);
        return root;
    }

    TreeNode* findMax(TreeNode* root) {
        if(root == NULL)
            return NULL;
        if(root->right)
            return findMax(root->right);
        return root;
    }

    vector<int> findMode(TreeNode* root) {
        long int min, max;
        vector<int> res;
        min = findMin(root)->val;
        max = findMax(root)->val;
        count_freq(root);
        num_nodes = max;
        for(int i=0; i<=max; i++)
            count.push_back(0);
        long int max_count = -1;
        for(long int i=min; i<=max; i++)
            if(count[i] > max_count)
                max_count = count[i];
        for(long int i=min; i<=max; i++)
            if(count[i] == max_count)
                res.push_back(i);
        return res;
    }
};

int main() {
    Solution S;
    TreeNode* root = new_node(1);
    root->left = new_node(2);
    root->right = new_node(2);
    root->left->left = new_node(3);
    root->left->right = new_node(4);
    root->right->left = new_node(5);
    root->right->right = new_node(5);
    vector<int> res = S.findMode(root);
    for(int i=0; i<res.size(); i++)
        cout << res[i] << " ";
    cout << endl;
    return 0;
}