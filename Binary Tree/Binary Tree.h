#include <iostream>
#include <stack>
#include <queue>

#define INORDER 1
#define PREORDER 2
#define POSTORDER 3
#define LEVELORDER 4

using namespace std;

class BinaryTree {
public:
	struct TreeNode {
		int data;
		TreeNode* left;
		TreeNode* right;
	};
	
	TreeNode* root;
	int size;

	TreeNode* new_node(int val, TreeNode* left=NULL, TreeNode* right=NULL) {
		TreeNode* new_ = new TreeNode;
		new_->data = val;
		new_->left = left;
		new_->right = right;
		return new_;
	}
	
	BinaryTree() {
		root = NULL;
		size = 0;
	}

	void tree_delete(TreeNode* root) {
		if(root == NULL)
			return;
		tree_delete(root->left);
		tree_delete(root->right);
		delete root;
	}

	~BinaryTree() {
		if(root == NULL)
			return;
		else
			tree_delete(root);
		root = NULL;
	}

	void inOrder(TreeNode* root) {
		if(root == NULL)
			return;
		inOrder(root->left);
		cout << root->data << " ";
		inOrder(root->right);
	}

	void preOrder(TreeNode* root) {
		if(root == NULL)
			return;
		cout << root->data << " ";
		preOrder(root->left);
		preOrder(root->right);
	}

	void postOrder(TreeNode* root) {
		if(root == NULL)
			return;
		postOrder(root->left);
		postOrder(root->right);
		cout << root->data <<  " ";
	}

	void levelOrder() {
		if(root == NULL)
			return;
		queue<TreeNode*> q;
		q.push(root);
		while(!q.empty()) {
			TreeNode* temp = q.front();
			cout << temp->data << " ";
			if(temp->left)
				q.push(temp->left);
			if(temp->right)
				q.push(temp->right); 
			q.pop();
		}
		cout << endl;
	}

	void print(int order=INORDER) {
		switch(order) {
			case INORDER:
				inOrder(root);
				break;
			case PREORDER: 	
				preOrder(root);
				break;
			case POSTORDER:	
				postOrder(root);
				break;
			case LEVELORDER:
				levelOrder();
				break; 
			default:
				cout << "Invalid option!"; 
		}
		cout << endl;
	}

	vector<int> k_dist_from_root(int k) {
		
	}
};

