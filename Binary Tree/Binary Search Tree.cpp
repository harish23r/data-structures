#include <iostream>
#include <cmath>
#include <climits>
#include <vector>
#include <stack>
#include <queue>

#define INORDER 1
#define PREORDER 2
#define POSTORDER 3
#define LEVELORDER 4

using namespace std;

struct list {
	int data;
	list* next;
};

class BST {
	// BST Node structure
	struct BSTNode {
		int data;
		BSTNode* left;
		BSTNode* right;
	};
	BSTNode* root;		// Root of the tree
	int nodes;			// Number of nodes
	int height;			// Current height of tree
	
	// Called by destructor
	BSTNode* deleteTree(BSTNode* root) {
		if(root == NULL)
			return NULL;
		deleteTree(root->left);
		deleteTree(root->right);
		delete root;
		return NULL;
	}

	// Allocates and returns a new BSTNode
	// Assigns NULL by default to left & right
	BSTNode* new_node(int val, BSTNode* left = NULL, BSTNode* right = NULL) {
		BSTNode* new_ = new BSTNode;
		new_->data = val;
		new_->left = left;
		new_->right = right;
		return new_;
	}

	// Private: Prints the inOrder sequence of nodes
	void inOrder(BSTNode* root) {
		if(root == NULL)
			return;
		inOrder(root->left);
		cout << root->data << " ";
		inOrder(root->right);
	}

	// Private: Prints the preOrder sequence of nodes
	void preOrder(BSTNode* root) {
		if(root == NULL)
			return;
		cout << root->data << " ";
		preOrder(root->left);
		preOrder(root->right);
	}

	// Private: Prints the postOrder sequence of nodes
	void postOrder(BSTNode* root) {
		if(root == NULL)
			return;
		postOrder(root->left);
		postOrder(root->right);
		cout << root->data <<  " ";
	}

	// Private: Prints the level order sequence of nodes
	void levelOrder() {
		if(root == NULL)
			return;
		queue<BSTNode*> q;
		q.push(root);
		while(!q.empty()) {
			BSTNode* temp = q.front();
			cout << temp->data << " ";
			if(temp->left)
				q.push(temp->left);
			if(temp->right)
				q.push(temp->right); 
			q.pop();
		}
		cout << endl;
	}

	BSTNode* findMax(BSTNode* root) {
		if(root == NULL)
			return NULL;
		BSTNode* node = root;
		while(node->right != NULL)
			node = node->right;
		return node;
	}

	BSTNode* findMin(BSTNode* root) {
		if(root == NULL)
			return NULL;
		BSTNode* node = root;
		while(node->left != NULL)
			node = node->left;
		return node;
	}

	BSTNode* findMax() {
		if(root == NULL)
			return NULL;
		BSTNode* node = root;
		while(node->right != NULL)
			node = node->right;
		return node;
	}

	BSTNode* findMin() {
		if(root == NULL)
			return NULL;
		BSTNode* node = root;
		while(node->left != NULL)
			node = node->left;
		return node;
	}

	int size(BSTNode* root) {
		if(root == NULL)
			return 0;
		return size(root->left) + size(root->right) + 1;
	}

	BSTNode* insert_key(BSTNode* node, int key) {
		if(node == NULL) 
			node = new_node(key, NULL, NULL);
		else if(key < node->data)
			node->left = insert_key(node->left, key);
		else if(key > node->data)
			node->right = insert_key(node->right, key);
		// Duplicate key
		else
				;	// Do nothing
		return node;
	}

	BSTNode* remove_key(BSTNode* root, int key) {
		if(root == NULL)
			return root;
		else {
			if(key < root->data) 
				root->left = remove_key(root->left, key);
			else if(key > root->data)
				root->right = remove_key(root->right, key);
			// Key found
			else {
				// Leaf node
				if(!root->left && !root->right) {
					delete root;
					return NULL;
				}
				// 2 children
				else if(root->left && root->right) {
					BSTNode* minNode = findMin();
					root->data = minNode->data;
					root->right = remove_key(root->right, minNode->data);
				}
				// 1 child
				else {
					BSTNode* temp = root;
					if(root->right) 
						root = root->right;
					else if(root->left)
						root = temp->left;
					delete temp;
				}
			}
		}
		return root;
	}

	bool exists(BSTNode* root, int key) {
		if(root == NULL)
			return false;
		if(root->data == key)
			return true;
		return (exists(root->left, key) || exists(root->right, key));
	}

	void printRange(BSTNode* root, int x, int y) {
		if(root == NULL)
			return;
		if(root->data < x)
			return;
		printRange(root->left, x, y);
		if(root->data > y)
			return;
		if(root->data <= y)
			cout << root->data << " ";
		printRange(root->right, x, y); 
	}

	void inOrderSuccessor(BSTNode* root, int key, BSTNode*& successor) {
		if(root == NULL)
			return;
		if(key < root->data) {
			// current root might be the IOS, if key lies in left subtree
			successor = root;
			inOrderSuccessor(root->left, key, successor);
		}
		else if(key > root->data) {
			inOrderSuccessor(root->right, key, successor);
		}
		else { // Key found
			// If right subtree exist, then min is IOS
			if(root->right)
				successor = findMin(root->right);
		}
	}

	void inOrderPredecessor(BSTNode* root, int key, BSTNode*& predecessor) {
		if(root == NULL)
			return;
		if(key < root->data) {
			inOrderPredecessor(root->left, key, predecessor);
		}
		else if(key > root->data) {
			// current root might be the IOP, if key lies in right subtree
			predecessor = root; 
			inOrderPredecessor(root->right, key, predecessor);
		}
		else { // Key found
			// If left subtree exist, then max is IOP
			if(root->left)
				predecessor = findMax(root->left);
		}
	}

	// construct from preorder
	BSTNode* fromSortedArray(vector<int>& a, int m, int n) { 
		if(m > n)
			return NULL;
		if(m == n) {
			BSTNode* root = new_node(a[m]);
			return root;
		}
		int mid = (m+n)/2;
		BSTNode* root = new_node(a[mid]);
		root->left = fromSortedArray(a, m, mid-1);
		root->right = fromSortedArray(a, mid+1, n);
		return root;
	}

	BSTNode* fromPreOrder(vector<int>& preOrder, int m, int n) { 
		if(n - m < 0)
			return NULL;
		if(m == n) {
			BSTNode* root = new_node(preOrder[m]);
			return root;
		}
		BSTNode* root = new_node(preOrder[m]);
		int i = m+1;
		while(preOrder[i] < preOrder[m])
			i++;
		root->left = fromPreOrder(preOrder, m+1, i-1);
		root->right = fromPreOrder(preOrder, i, n);
		return root;
	}

public:
	BST() {
		root = NULL;
		nodes = 0;
	}

	// Construct Balanced BST from sorted array
	BST(vector<int> a, int b) {
		int len = a.size();
		root = fromSortedArray(a, 0, len-1);
	}

	// Construct BST from PreOrder
	BST(vector<int> a) {
		int len = a.size();
		root = fromPreOrder(a, 0, len-1);
	}
	
	// Construct Balanced BST from sorted List
	BST(list* head) {
		root = NULL;
	}

	// Destructor: Destroy all nodes.
	// Calls delete_all
	~BST() {
		root = deleteTree(root);
	}

	// Insert a particular value into BST, do nothing if duplicate
	void insert(int key) {
		this->root = insert_key(this->root, key);
		nodes = size(root);
	}

	// Delete a value from the BST, do nothing if element doesn't exist.
	void remove(int key) {
		root = remove_key(root, key);
		nodes = size(root);
	}

	// Returns the node with the key, NULL if key doesn't exist
	/*BSTNode* search(int key) {
		BSTNode* node = search_key(root, key);
		return node;
	}*/

	// Returns true if key exist in tree, false otherwise
	bool exist(int key) {
		return exist(key);
	}

	// Returns number of nodes in tree
	int size() {
		return nodes;
	}

	int findHeight(BSTNode* root) {
		if(root == NULL)
			return 0;
		return std::max(findHeight(root->left), findHeight(root->right)) + 1;
	}

	// Returns the current height of the tree
	// int height() { return this->height; }

	// Returns the minimum value in tree
	int min() {
		BSTNode* node = findMin(root);
		return node ? node->data : INT_MIN;
	}

	// Returns the maximum value in tree
	int max() {
		BSTNode* node = findMax(root);
		return node ? node->data : INT_MAX;
	}

	// Find the kth largest element (Order Statstics)
	// int kthMax() {}

	// Find the kth smallest element (Order Statistics)
	// int kthMin() {}

	// Assumes, x and y is preset in the tree
	void range(int x, int y) {
		if(root == NULL)
			return;
		printRange(root, x, y);
		cout << endl;
	}

	void inOrderSuccessor(int key) {
		// If key doesn't return, report error
		if(!exists(root, key)) {
			cout << "Key " << key << " is not in the tree" << endl;
			return;
		}
		// Initialize successor, pass it to inOrderSuccessor()
		BSTNode* successor = NULL;
		inOrderSuccessor(root, key, successor);
		if(successor != NULL)
			cout << "IOS(" << key << "): " << successor->data << endl;
		else
			cout << "No Successor" << endl;
	}

	void inOrderPredecessor(int key) {
		// If key doesn't return, report error
		if(!exists(root, key)) {
			cout << "Key " << key << " is not in the tree" << endl;
			return;
		}
		// Initialize predecessor, pass it to inOrderPredecessor
		BSTNode* predecessor = NULL;
		inOrderPredecessor(root, key, predecessor);
		if(predecessor != NULL)
			cout << "IOP(" << key << "): " << predecessor->data << endl;
		else
			cout << "IOP(" << key << "): " << "No Predecessor" << endl;
	}

	void print(int order=INORDER) {
		switch(order) {
			case INORDER:
				inOrder(root);
				break;
			case PREORDER: 	
				preOrder(root);
				break;
			case POSTORDER:	
				postOrder(root);
				break;
			case LEVELORDER:
				levelOrder();
				break; 
			default:
				cout << "Invalid option!"; 
		}
		cout << endl;
	}
};

int main() {
	BST bst;
	bst.insert(5);
	bst.insert(3);
	bst.insert(2);
	bst.insert(1);
	bst.insert(4);
	bst.insert(8);
	bst.insert(7);
	bst.insert(9);
	bst.print(INORDER);
	bst.print(PREORDER);
	bst.print(POSTORDER);
	bst.print(LEVELORDER);
	bst.range(3,8);
	bst.inOrderSuccessor(5);
	bst.inOrderSuccessor(9);
	bst.inOrderSuccessor(7);
	bst.inOrderSuccessor(3);
	bst.inOrderSuccessor(4);
	bst.inOrderSuccessor(8);
	bst.inOrderSuccessor(1);
	bst.inOrderSuccessor(2);
	bst.inOrderPredecessor(1);
	bst.inOrderPredecessor(2);
	bst.inOrderPredecessor(5);
	bst.inOrderPredecessor(8);
	bst.inOrderPredecessor(9);
	// cout << "Max: " << bst.max() << endl;
	// cout << "Min: " << bst.min() << endl;
	vector<int> preOrder;
	preOrder.push_back(10);
	preOrder.push_back(5);
	preOrder.push_back(1);
	preOrder.push_back(7);
	preOrder.push_back(40);
	preOrder.push_back(50);
	BST pre(preOrder);
	cout << "Constructor" << endl;
	pre.print(INORDER);
	pre.print(LEVELORDER);
	vector<int> array;
	array.push_back(1); 
	array.push_back(2);
	array.push_back(3);
	array.push_back(4);
	array.push_back(5);
	array.push_back(6);
	array.push_back(7);
	BST fromArray(array, 0);
	fromArray.print(INORDER);
	fromArray.print(LEVELORDER);
	return 0;
}