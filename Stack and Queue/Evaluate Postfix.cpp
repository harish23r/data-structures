#include <iostream>
#include <stack>
using namespace std;

/* 	Evaluation of postfix expression using stack
	Limited to single digit integers & arithmetic operators + - * / */
int eval_postfix(string postfix) {
	int a, b, c;
	stack<int> s;
	int len = postfix.size();
	for(int i=0; i < len; i++) {
		// If number, push into stack
		if(int(postfix[i]) >= 48 && int(postfix[i]) <= 57) {
			s.push(postfix[i] - 48);
			cout << s.top() << endl;
		}
		// Else, operator. Pop last two elements, perform operation, push back result
		else {
			cout << s.top() << endl; a = s.top(); s.pop();
			cout << s.top() << endl; b = s.top(); s.pop();
			switch(char(postfix[i])) {
				case '+':s.push(a + b); 
							break;
				case '-': s.push(a - b);
						break;
				case '*': s.push(a * b);
						break;
				case '/': s.push(a / b);
						break;
			}
		}
	}   
	return s.top();
}

int main() { 
	string s = "6523+8*+3+*";
	cout << eval_postfix(s) << endl;
	return 0;
}